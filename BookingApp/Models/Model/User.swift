//
//  User.swift
//  BookingApp
//
//  Created by Zenchef on 03/05/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import Foundation

struct Name:Decodable{
    var first: String?
    var last: String?
}


struct User {
    var name: Name?
    var gender: Gender?
    var email: String?
    var phone: String?
}

enum Gender: String {
    case male
    case female
}

extension User:Decodable {
    
    enum UserCodingKeys: String, CodingKey {
        case name
        case email
        case phone
    }
    
    init(from decoder: Decoder) throws {
        let userContainer = try decoder.container(keyedBy: UserCodingKeys.self)
        name = try userContainer.decode(Name.self, forKey: .name)
        email = try userContainer.decode(String.self, forKey: .email)
        phone = try userContainer.decode(String.self, forKey: .phone)
    }
}
