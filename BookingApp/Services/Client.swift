//
//  Client.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import Foundation
enum NetworkError: Error {
    case badURL
}

class Client {
    func getBookings(_ maxBooking: Int,completionHandler: @escaping (Result<[Booking], NetworkError>) -> Void = {_ in }){
        
        let urlString : String = "https://randomuser.me/api/?results=25"
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: urlString)
        request.httpMethod = "GET"
        var Bookings:[Booking] = []
        guard URL(string: urlString) != nil else {
               completionHandler(.failure(.badURL))
               return
           }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
            print(response!)
            guard URL(string: urlString) != nil else {
                   completionHandler(.failure(.badURL))
                   return
               }
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
                let apiResponse = try JSONDecoder().decode(UsersResponseResponse.self, from: data!)
                Bookings =  apiResponse.users.map{Booking(user: $0)}
                completionHandler(.success(Bookings))
                
            } catch {
                print("error")
            }
        })

        task.resume()

        // Call API GET to https://randomuser.me/api/?results=XXX to retrieve an array of User.
        // Then create an array of bookings from the array of users
        //return Bookings
    }
}
