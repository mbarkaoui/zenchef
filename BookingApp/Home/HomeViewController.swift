//
//  ViewController.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    let label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home"
        
      let client = Client()
      client.getBookings(25,completionHandler: {
            result in
            switch result {
            case .success(let bookings):
                print("\(bookings) Bookings .")
                DispatchQueue.main.async {
                    self.view.backgroundColor = bookings.first?.status.color
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        })
      
    }
        
}
