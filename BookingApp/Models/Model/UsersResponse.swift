//
//  UsersResponse.swift
//  BookingApp
//
//  Created by Malek BARKAOUI on 11/05/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import Foundation

struct UsersResponseResponse {
    let users: [User]
}

extension UsersResponseResponse: Decodable {
    
    private enum UsersResponseCodingKeys: String, CodingKey {
        case users = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UsersResponseCodingKeys.self)
        users = try container.decode([User].self, forKey: .users)
    }
}
